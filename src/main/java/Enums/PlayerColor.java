package Enums;

public enum PlayerColor
{
    Black(-1), White(1);

    private int idx;

    PlayerColor(int idx) //indexes of the enum will be used when moving pawns
    {
        this.idx = idx;
    }

    public int getidx()
    {
        return idx;
    }
}
