package Playfield;

public class Board
{

    /*
    board will be positioned as:
      Y
    (7,0) .. (7,7)
      .        .
      .        .
    (0,0) .. (0,7) X

    on rows 0 and 1 white player starts
    on rows 6 and 7 black player starts
     */
    public static Square[][] board = new Square[8][8];
}
