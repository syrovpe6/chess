package Pieces;

import Playfield.Board;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;


public class King extends Piece
{
    boolean castlingPossible = true;

    @Override
    public List<Point> possibleMoves()
    {
        List<Point> possibleMoves = new ArrayList<>();
        /*
        !!! ADD ALSO RECOGNITION FOR NOT STEPPING ON ATTACKED SQUARE
         */
        for (int i = -1; i <= 1; i++)
        {
            for (int j = -1; j <= 1; j++)
            {
                Piece currPiece = Board.board[point.x += i][point.y += j].piece;
                //is not current position of King and {is empty or is occupied by enemy piece}
                if ((i != 0 && j != 0) && (currPiece == null || currPiece.playerColor != playerColor))
                {
                    possibleMoves.add(currPiece.point);
                }
            }
        }
        return possibleMoves;
    }
}
