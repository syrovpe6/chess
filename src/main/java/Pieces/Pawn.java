package Pieces;

import Enums.PlayerColor;
import Playfield.Board;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Pawn extends Piece
{
    boolean moved = false; //if possible to move two squares ahead


    @Override
    List<Point> possibleMoves()
    {
        List<Point> possibleMoves = new ArrayList<>();

        if (point.y > 0 && point.y < 7 && Board.board[point.x][point.y + playerColor.getidx()].piece == null)
        {
            possibleMoves.add(new Point(point.x, point.y + playerColor.getidx()));
            if (moved == false && Board.board[point.x][point.y + 2 * playerColor.getidx()].piece == null)
            {
                possibleMoves.add(new Point(point.x, point.y + 2 * playerColor.getidx()));
            }
        }
        for (int i = -1; i <= 1; i += 2)
        {
            Piece currPiece = Board.board[point.x + i][point.y + playerColor.getidx()].piece;
            //square is occupied by enemy piece
            if (currPiece != null && currPiece.playerColor != playerColor)
            {
                possibleMoves.add(currPiece.point);
            }
        }
        return possibleMoves;
    }
}
