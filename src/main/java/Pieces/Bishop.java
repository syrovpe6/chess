package Pieces;

import java.awt.*;
import java.io.PipedReader;
import java.util.ArrayList;
import java.util.List;

import Playfield.Board;

public class Bishop extends Piece
{
    @Override
    public List<Point> possibleMoves()
    {
        List<Point> possibleMoves = new ArrayList<>();
        for (int i = -1; i <= 1; i += 2)  //-1-1, -11, 1-1, 11
        {
            for (int j = -1; j <= 1; j += 2)
            {
                Point currPoint = (Point) point.clone();
                //is located in point, from where it makes sense to try to go further
                while (7 > currPoint.x && currPoint.x > 0 && currPoint.y > 0 && currPoint.y < 7)
                {
                    Piece currentPiece = Board.board[currPoint.x += i][currPoint.y += j].piece;
                    if (currentPiece == null) //space is empty
                    {
                        possibleMoves.add(new Point(currPoint.x, currPoint.y));
                    } else if (currentPiece.playerColor != playerColor) //space is occupied by enemy piece
                    {
                        possibleMoves.add(new Point(currPoint.x, currPoint.y));
                        break;
                    } else
                    {
                        break; //is occupied by my piece
                    }
                }
            }
        }

        return possibleMoves;
    }
}
