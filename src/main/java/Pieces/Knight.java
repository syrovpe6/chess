package Pieces;

import Playfield.Board;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Knight extends Piece
{
    @Override
    List<Point> possibleMoves()
    {
        List<Point> possibleMoves = new ArrayList<>();

        //first two cycles cycle through all possible permutations of move shifts
        //third iteration decide the application of permutation on checked square
        for (int i = -1; i <= 1; i += 2)
        {
            for (int j = -2; j <= 2; j += 4)
            {
                for (int k = 0; k <= 1; k++)
                {
                    Point currPoint;
                    if (k == 0)
                    {
                        currPoint = new Point(point.x + i, point.y + j);
                    } else
                    {
                        currPoint = new Point(point.x + j, point.y + i);
                    }

                    if (currPoint.x >= 0 && currPoint.x <= 7 && point.y >= 0 && point.y <= 7 &&)
                    {
                        Piece currPiece = Board.board[currPoint.x][currPoint.y].piece;
                        if (currPiece != null && currPiece.playerColor != playerColor)
                        {
                            possibleMoves.add(currPoint);
                        }
                    }
                }
            }
        } return possibleMoves;
    }
}
