package Pieces;

import Enums.PlayerColor;

import java.awt.*;
import java.util.List;

public abstract class Piece
{
    PlayerColor playerColor;
    Point point;

    /**
     * @return array of Points of possible moves
     */
    List<Point> possibleMoves()
    {
        return null;
    }
}
