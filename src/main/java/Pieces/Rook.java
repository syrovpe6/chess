package Pieces;

import Playfield.Board;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Rook extends Piece
{
    @Override
    public List<Point> possibleMoves()
    {
        List<Point> possibleMoves = new ArrayList<>();

        for (int i = -1; i <= 1; i += 2)
        {
            Point currPoint = (Point) point.clone();
            while (currPoint.x > 0 && currPoint.x < 7) //iteration in horizontal direction
            {
                Piece currPiece = Board.board[currPoint.x += i][currPoint.y].piece;
                if (currPiece == null) //space is empty
                {
                    possibleMoves.add(new Point(currPoint.x, currPoint.y));
                } else if (currPiece.playerColor != playerColor) //space is occupied by enemy piece
                {
                    possibleMoves.add(new Point(currPoint.x, currPoint.y));
                    break;
                } else
                {
                    break; //is occupied by my piece
                }
            }
            currPoint = (Point) point.clone();
            while (currPoint.y > 0 && currPoint.y < 7) //iteration in vertical direction
            {
                Piece currPiece = Board.board[currPoint.x][currPoint.y += i].piece;
                if (currPiece == null) //space is empty
                {
                    possibleMoves.add(new Point(currPoint.x, currPoint.y));
                } else if (currPiece.playerColor != playerColor) //space is occupied by enemy piece
                {
                    possibleMoves.add(new Point(currPoint.x, currPoint.y));
                    break;
                } else
                {
                    break; //is occupied by my piece
                }
            }

        }

        return possibleMoves;
    }
}
